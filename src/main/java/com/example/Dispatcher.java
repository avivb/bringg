package com.example;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

public interface Dispatcher<T> {
    /**
     * Dispatch a given callables see {@link Callable}
     * @param callables
     * @return list of futures in order to get computed results
     * @throws InterruptedException
     */
    List<Future<T>> dispatch(List<Callable<T>> callables) throws InterruptedException;

    /**
     * closes and shutting down all dispatcher resources
     */
    void shutdown();
}
