package com.example.helpers;

import com.example.pojos.Point;

public class Mathematics {

    /**
     * Calculate distance between point1 and point2
     * @param point1
     * @param point2
     * @return - the distance
     */
    public static double calculateDistanceBetweenPoints(Point point1, Point point2) {
        return Math.sqrt((point2.getY() - point1.getY()) * (point2.getY() - point1.getY()) + (point2.getX() - point1.getX()) * (point2.getX() - point1.getX()));
    }
}
