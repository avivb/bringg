package com.example.helpers;

import java.util.List;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

public class FutureHelper {

    /**
     * wait till all futures are completed
     * In case some futures will fail, it will be ignored and stack trace will be printed to standard error
     * @param futures - list of futures
     * @return list of values of these futures
     */
    public static <T> List<T> waitForAllFutures(List<Future<T>> futures) {
        return futures.stream()
                .map(taskResultFuture -> {
                    try {
                        return taskResultFuture.get();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return null;
                })
                .collect(Collectors.toList());
    }
}
