package com.example;

import com.example.commandline.CalcMatrixDistanceCommand;
import picocli.CommandLine;

public class Main {

    public static void main(String... args) {
        int exitCode = new CommandLine(new CalcMatrixDistanceCommand()).execute(args);
        System.exit(exitCode);
    }

}
