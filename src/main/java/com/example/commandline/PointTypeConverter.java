package com.example.commandline;

import com.example.pojos.Point;
import picocli.CommandLine;

public class PointTypeConverter implements CommandLine.ITypeConverter<Point> {

    @Override
    public Point convert(String s) throws Exception {
        String[] fields = s.split(",");
        return new Point(
                Double.parseDouble(fields[0]), // X
                Double.parseDouble(fields[1]) // Y
        );
    }
}
