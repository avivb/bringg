package com.example.commandline;

import com.example.MatrixCalculator;
import com.example.impl.MatrixCalculatorImpl;
import com.example.pojos.Point;
import picocli.CommandLine;

import java.util.concurrent.Callable;

@CommandLine.Command()
public class CalcMatrixDistanceCommand implements Callable<Void> {

    private MatrixCalculator matrixCalculator = new MatrixCalculatorImpl();

    @CommandLine.Parameters(index = "0", description = "Number of executing threads")
    private int numOfThreads;

    @CommandLine.Parameters(arity = "1..*", index = "1..*", description = "List points in the format of 'x,y' example 4,1", converter = PointTypeConverter.class)
    private Point[] points;

    @Override
    public Void call() throws Exception {
        double[][] matrix = matrixCalculator.calcDistanceMatrix(points, numOfThreads);
        printGrid(matrix);
        return null;
    }

    private void printGrid(double[][] matrix) {
        for(int i = 0; i < matrix.length; i++) {
            System.out.printf("|  ");
            for(int j = 0; j < matrix.length; j++) {
                System.out.printf("%.1f  |  ", matrix[i][j]);
            }
            System.out.println();
        }
    }
}
