package com.example.impl;

import com.example.Dispatcher;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ThreadBalancedDispatcherImpl<T> implements Dispatcher<T> {
    private final int numOfThreads;
    private final List<ExecutorService> executorServices;

    public ThreadBalancedDispatcherImpl(int numOfThreads) {
        this.numOfThreads = numOfThreads;
        this.executorServices = new ArrayList<>();
        for (int i=0; i < numOfThreads; i++) {
            executorServices.add(Executors.newSingleThreadExecutor());
        }
    }

    @Override
    public List<Future<T>> dispatch(List<Callable<T>> callables) throws InterruptedException {

        int amountOnEachGroup = (int) Math.ceil(numOfThreads / callables.size());
        List<Future<T>> futures = new ArrayList<>();

        for (int i=0; i < numOfThreads; i++) {
            boolean stopIterating = false;
            int fromIndex = i * amountOnEachGroup;
            int toIndex = fromIndex + amountOnEachGroup;
            if (toIndex > callables.size() - 1) {
                toIndex = callables.size() - 1;
                stopIterating = true;
            }
            futures.addAll(
                    executorServices.get(i).invokeAll(
                            callables.subList(fromIndex, toIndex)
                    )
            );
            if (stopIterating) break;
        }

        return futures;
    }

    @Override
    public void shutdown() {
        executorServices.forEach(ExecutorService::shutdown);
    }
}
