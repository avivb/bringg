package com.example.impl;

import com.example.pojos.TaskRequest;
import com.example.pojos.TaskResult;

import java.util.concurrent.Callable;

import static com.example.helpers.Mathematics.calculateDistanceBetweenPoints;

public class TaskCallable implements Callable<TaskResult> {

    private final TaskRequest taskRequest;

    public TaskCallable(TaskRequest taskRequest) {
        this.taskRequest = taskRequest;
    }

    @Override
    public TaskResult call() {
        return new TaskResult(calculateDistanceBetweenPoints(taskRequest.getPoint1(), taskRequest.getPoint2()), taskRequest.getMatrixPosition());
    }
}
