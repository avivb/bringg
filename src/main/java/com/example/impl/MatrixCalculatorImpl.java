package com.example.impl;

import com.example.Dispatcher;
import com.example.MatrixCalculator;
import com.example.pojos.MatrixPosition;
import com.example.pojos.Point;
import com.example.pojos.TaskRequest;
import com.example.pojos.TaskResult;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import static com.example.helpers.FutureHelper.waitForAllFutures;

public class MatrixCalculatorImpl implements MatrixCalculator {

    @Override
    public double[][] calcDistanceMatrix(Point[] points, int numOfThreads) throws InterruptedException {

        Dispatcher<TaskResult> dispatcher = new ThreadBalancedDispatcherImpl(numOfThreads);

        List<Callable<TaskResult>> taskRequests = createTaskRequests(points);

        List<Future<TaskResult>> futures = dispatcher.dispatch(taskRequests);

        List<TaskResult> taskResults = waitForAllFutures(futures);

        dispatcher.shutdown();

        return buildMatrix(taskResults, points.length);
    }

    private List<Callable<TaskResult>> createTaskRequests(Point[] points) {
        List<Callable<TaskResult>> taskRequests = new ArrayList<>();
        for (int column=0; column < points.length; column++) {
            for (int row=0; row < points.length; row++) {
                TaskRequest taskRequest = new TaskRequest(points[column], points[row], new MatrixPosition(column, row));
                taskRequests.add(new TaskCallable(taskRequest));
            }
        }
        return taskRequests;
    }

    private double[][] buildMatrix(List<TaskResult> taskResults, int pointsAmount) {
        double[][] matrix = new double[pointsAmount][pointsAmount];

        taskResults.forEach(taskResult -> {
            int row = taskResult.getMatrixPosition().getRow();
            int column = taskResult.getMatrixPosition().getColumn();

            matrix[row][column] = taskResult.getDistance();
        });

        return matrix;
    }
}
