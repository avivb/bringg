package com.example.pojos;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Data
public class TaskRequest {
    private final Point point1;
    private final Point point2;
    private final MatrixPosition matrixPosition;
}
