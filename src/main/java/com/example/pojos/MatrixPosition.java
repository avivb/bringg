package com.example.pojos;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Data
public class MatrixPosition {
    private final int row;
    private final int column;
}
