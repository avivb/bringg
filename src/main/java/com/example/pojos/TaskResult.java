package com.example.pojos;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Data
public class TaskResult {
    private final double distance;
    private final MatrixPosition matrixPosition;
}
