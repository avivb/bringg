package com.example.pojos;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Data
public class Point {
    private final double x;
    private final double y;
}
