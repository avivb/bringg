package com.example;

import com.example.pojos.Point;

public interface MatrixCalculator {
    /**
     * Calculate distance matrix between all given points (all possible options will be calculated)
     * This method can uses several threads in order to parallelize this calculation
     * @param points - array of points
     * @param numOfThreads - num of threads
     * @return - matrix with all distances
     * @throws InterruptedException
     */
    double[][] calcDistanceMatrix(Point[] points, int numOfThreads) throws InterruptedException;
}
