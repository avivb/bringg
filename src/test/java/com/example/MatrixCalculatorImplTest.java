package com.example;

import com.example.impl.MatrixCalculatorImpl;
import com.example.pojos.Point;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class MatrixCalculatorImplTest {

    private static MatrixCalculator matrixCalculator;

    @BeforeClass
    public static void setup() {
        matrixCalculator = new MatrixCalculatorImpl();
    }

    @Test
    public void test_calcDistanceMatrix() throws InterruptedException {
        double[][] matrix = matrixCalculator.calcDistanceMatrix(new Point[] {new Point(2,-1), new Point(-2,2)}, 10);
        Assert.assertEquals("Unexpected value", matrix[0][0], 0, 0);
        Assert.assertEquals("Unexpected value", matrix[0][1], 5, 0);
        Assert.assertEquals("Unexpected value", matrix[1][0], 5, 0);
        Assert.assertEquals("Unexpected value", matrix[1][1], 0, 0);
    }
}
